<?php
$servername = "localhost";
$database = "prueba";
$username = "root";
$password = "root";
// Create connection
$conn = mysqli_connect($servername, $username, $password, $database);
// Check connection
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}
//echo "Connected successfully";

?>

<?php

    if (isset($_POST['action'])) {
     select();
    }

    function select() {
      $servername = "localhost";
      $database = "prueba";
      $username = "root";
      $password = "root";
      $conn = mysqli_connect($servername, $username, $password, $database);
      $consulta = "SELECT Peliculas.nombre, Peliculas.genero, Directores.FirstName, Directores.LastName, Productora.nombre, Productora.anio_creacion FROM Peliculas INNER JOIN Directores ON Directores.id = Peliculas.id_director INNER JOIN Productora ON Productora.id = Peliculas.id_productora";
      $resultado = mysql_query($consulta,$conn);
    }
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>Prueba 2022</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>

<body>

    <div class="jumbotron text-center">
        <h1>Pagina de prueba </h1>
        <img class="img-fluid" style="size: auto"
            src="https://clubmemes.com/wp-content/uploads/2020/05/98484479_1636779833152242_7810055598623424512_n.jpg">
    </div>

    <div class="container">
        <div class="row text-center">
            <div class="col-sm-12 col-md-12 col-12">
                <div class="row">
                    <!--<button type="button" class="btn btn-success">Click para Conexion SQL</button>-->
                    <form method="post" action="index.php">
                        <input type="submit" class="btn btn-danger button" name="select" value="Click Para Ver SQL" />
                    </form>
                </div>
            </div>
        </div>
        <div class="row">
            <table id="example" class="display" style="width:100%">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Genero</th>
                        <th>Director</th>
                        <th>Apellido Director</th>
                        <th>Casa Productora</th>
                        <th>Fecha</th>
                    </tr>
                </thead>

                <?php
                  foreach ($resultado as $row) {
                    echo "<tr>";
                    echo "<th>".$row[0]."</th>";
                    echo "<th>".$row[1]."</th>";
                    echo "<th>".$row[2]."</th>";
                    echo "<th>".$row[3]."</th>";
                    echo "<th>".$row[4]."</th>";
                    echo "<th>".$row[5]."</th>";
                    echo "</tr>";
                  }
                ?>
                </tbody>
                <tfoot>
                    <tr>
                        <th>Nombre</th>
                        <th>Genero</th>
                        <th>Director</th>
                        <th>Apellido Director</th>
                        <th>Casa Productora</th>
                        <th>Fecha</th>
                    </tr>
                </tfoot>
            </table>

        </div>
    </div>
    <script>
    $(document).ready(function() {
        $('.button').click(function() {

            var clickBtnValue = $(this).val();
            var ajaxurl = 'index.php',
                data = {
                    'action': clickBtnValue
                };
            $.post(ajaxurl, data, function(response) {
                // Response div goes here.
                alert("action performed successfully");
            });
        });
    });

    $(document).ready(function() {
        $('#example').DataTable();
    });
    </script>
</body>

</html>