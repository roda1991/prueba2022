-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:8889
-- Tiempo de generación: 27-10-2022 a las 19:33:29
-- Versión del servidor: 5.7.34
-- Versión de PHP: 7.4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `prueba`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Directores`
--

CREATE TABLE `Directores` (
  `id` int(11) DEFAULT NULL,
  `LastName` varchar(255) DEFAULT NULL,
  `FirstName` varchar(255) DEFAULT NULL,
  `Address` varchar(255) DEFAULT NULL,
  `City` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `Directores`
--

INSERT INTO `Directores` (`id`, `LastName`, `FirstName`, `Address`, `City`) VALUES
(1, 'Nolan', 'Christopher', 'Hollywood', 'Inglaterra'),
(2, 'Jackson', 'Peter', 'Hollywood', 'Nueva Zelanda'),
(3, 'Kosinski', 'Joseph', 'Hollywwod', 'Estados Unidos'),
(4, 'Sanchez', 'Eduardo', 'La Havana', 'Cuba'),
(5, 'Webb', 'Marc', 'Hollywwod', 'Estados Unidos'),
(6, 'Cameron', 'James', 'Hollywwod', 'Canada'),
(7, 'Estrada', 'Luis', 'Mexico', 'Mexico');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Peliculas`
--

CREATE TABLE `Peliculas` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `genero` varchar(255) NOT NULL,
  `id_director` int(11) NOT NULL,
  `id_productora` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `Peliculas`
--

INSERT INTO `Peliculas` (`id`, `nombre`, `genero`, `id_director`, `id_productora`) VALUES
(1, 'Interstellar', 'Space Opera', 1, 1),
(2, 'Trilogía cinematográfica de El Señor de los Anillos', 'Aventuras', 2, 2),
(3, 'A Nightmare on Elm Street', 'Terror', 3, 2),
(4, 'Top Gun Maverick', 'Accion', 4, 2),
(5, 'The Dark Knight', 'Super Heroes', 1, 3),
(6, 'The Flash', 'Super Heroes', 4, 4),
(7, 'The Blair Witch Project', 'Terror', 5, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Productora`
--

CREATE TABLE `Productora` (
  `id` int(11) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `ciudad` varchar(255) DEFAULT NULL,
  `anio_creacion` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `Productora`
--

INSERT INTO `Productora` (`id`, `nombre`, `ciudad`, `anio_creacion`) VALUES
(1, 'Paramount Pictures', 'Hollywood', '1912'),
(2, 'New Line Cinema', 'Estados Unidos', '1996'),
(3, 'DM FILMS', 'Estados Unidos', '2020'),
(4, 'Haxan Films', 'Orlando Florida', '1926'),
(5, 'Bandidos Films', 'CDMX', '1991');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `Peliculas`
--
ALTER TABLE `Peliculas`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `Peliculas`
--
ALTER TABLE `Peliculas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
